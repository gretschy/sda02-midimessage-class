//
//  MidiMessage.cpp
//  CommandLineTool
//
//  Created by Tom on 29/09/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#include "MidiMessage.hpp"
#include <iostream>
#include <cmath>

using namespace std;

MidiMessage::MidiMessage() //constructor
    {
        number = 60;
        velocity = 127;
        channel = 1;
        cout << "MidiMessage created \n";
    }

MidiMessage::MidiMessage(int startingNote, float startingVelocity, int startingChannel) //second constructor
    {
        number = startingNote;
        velocity = startingVelocity;
        channel = startingChannel;
    }

MidiMessage::~MidiMessage() //destructor
    {
        cout << "MidiMessage destroyed \n";
    }

//note number member functions
void MidiMessage::setNoteNumber (int value) //note mutator
    {
        if (value <= -1)
        {
            cout << "Your note number value is out of range. Please try again. \n";
        }
        
        else if (value >= 128)
        {
            cout << "Your note number value is out of range. Please try again. \n";
        }
        
        else
        {
            number = value;
        }
    }
    
int MidiMessage::getNoteNumber() const //note accessor
    {
        return number;
    }
    
float MidiMessage::getMidiNoteInHertz () const //note accessor
    {
        return 440 * pow (2, (number-69)/12.0);
    }
    
//velocity member functions
void MidiMessage::setVelocity (float value) //velocity mutator
    {
        if (value <= -1)
        {
            cout << "Your velocity value is out of range. Please try again. \n";
        }
        
        else if (value >= 128)
        {
            cout << "Your velocity value is out of range. Please try again. \n";
        }
        
        else
        {
            velocity = value/127.0;
        }
        
    }
    
float MidiMessage::getVelocity() const //velocity accessor
    {
        return velocity;
    }
    
//channel member functions
void MidiMessage::setChannel (int value) //channel mutator
    {
        if (value <= -1)
        {
            cout << "Your channel value is out of range. Please try again. \n";
        }
        else if (value >= 17)
        {
            cout << "Your velocity value is out of range. Please try again. \n";
        }
        else
        {
            channel = value;
        }
    }
    
int MidiMessage::getChannel() const //channel accessor
    {
        return channel;
    }

