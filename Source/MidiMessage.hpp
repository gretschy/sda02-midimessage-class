//
//  MidiMessage.hpp
//  CommandLineTool
//
//  Created by Tom on 29/09/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#ifndef MidiMessage_hpp
#define MidiMessage_hpp

#include <stdio.h>

class MidiMessage
{
public:
    MidiMessage(); /**constructor*/
    MidiMessage(int startingNote, float startingVelocity, int startingChannel); /**second constructor*/
    
    ~MidiMessage(); /**destructor*/
   
    /**note number member functions*/
    void setNoteNumber (int value);     /**note mutator*/
    int getNoteNumber() const;          /**note accessor*/
    float getMidiNoteInHertz () const;  /**note accessor*/
    
    /**velocity member functions*/
    void setVelocity (float value);     /**velocity mutator*/
    float getVelocity() const;          /**velocity accessor*/
    
    /**channel member functions*/
    void setChannel (int value);        /**channel mutator*/
    int getChannel() const;             /**channel accessor*/
 
    
private:
    int number;
    float velocity;
    int channel;
};


#endif /* MidiMessage_hpp */
